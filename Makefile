docker/build:
	@ docker image build -t micro8734/ci .

docker/start: docker/build
	@ docker-compose up -d

docker/stop:
	@ docker-compose down -v


docker/ps:
	@ docker-compose ps

command ?= 'sh'

docker/exec:
	@ docker-compose exec jenkins $(command)
